import Model from './model'

const NO_ID_MESSAGE = 'Failed! Instance should have id';

export default class AgilePrinciple extends Model {
  constructor(props = {}) {
    const defaultProps = {
      id: null,
      principle: 'no principle',
    }

    super({...defaultProps, ...props});
  }

  static fetchAll() {
    return axios.get(laroute.route('principles.index'))
      .then(({data}) => data.map((props) => new AgilePrinciple(props)))
      .catch(e => Log.error(e));
  }

  store() {
    return axios.post(laroute.route('principles.store'), {
      principle: this.principle,
    });
  }

  update() {
    if (this.id === null) {
      Log.info(NO_ID_MESSAGE);
      return new Promise((res, reject) => reject(NO_ID_MESSAGE));
    }

    return axios.patch(laroute.route('principles.update'), {
      id: this.id,
      principle: this.principle,
    });
  }

  destroy() {
    if (this.id === null) {
      Log.info(NO_ID_MESSAGE);
      return new Promise((res, reject) => reject(NO_ID_MESSAGE));
    }

    return axios.delete(laroute.route('principles.destroy'), {
      data: {
        id: this.id,
      }
    });
  }
}
