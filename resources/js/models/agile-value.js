import Model from './model'

const NO_ID_MESSAGE = 'Failed! Instance should have id';

export default class AgileValue extends Model {
  constructor(props = {}) {
    const defaultProps = {
      id: null,
      value: 'no value',
      over: 'empty',
    }

    super({...defaultProps, ...props});
  }

  static fetchAll() {
    return axios.get(laroute.route('values.index'))
      .then(({data}) => data.map((props) => new AgileValue(props)))
      .catch(e => Log.error(e));
  }

  store() {
    return axios.post(laroute.route('values.store'), {
      value: this.value,
      over: this.over,
    });
  }

  update() {
    if (this.id === null) {
      Log.info(NO_ID_MESSAGE);
      return new Promise((res, reject) => reject(NO_ID_MESSAGE));
    }

    return axios.patch(laroute.route('values.update'), {
      id: this.id,
      value: this.value,
      over: this.over,
    });
  }

  destroy() {
    if (this.id === null) {
      Log.info(NO_ID_MESSAGE);
      return new Promise((res, reject) => reject(NO_ID_MESSAGE));
    }

    return axios.delete(laroute.route('values.destroy'), {
      data: {
        id: this.id,
      }
    });
  }
}
