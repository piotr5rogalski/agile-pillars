import $store from '../store/index'
import Voca from 'voca'

const getSchemaName = name => {
  return Voca.snakeCase(name + 's');
}

const getRepo = name => {
  return $store.state.schemas[getSchemaName(name)];
}

export default class Model {
  constructor(props) {
    Object.keys(props).forEach(key => {
      this[key] = props[key];
    });
  }

  save() {
    VuexStore.commit('saveRecord', {
      schema: getSchemaName(this.constructor.name),
      record: this
    });
  }

  update(props) {
    Object.keys(props).forEach(key => {
      if (typeof this[key] !== 'undefined') {
        this[key] = props[key];
      } else {
        Log.error('Can\'t update property if not exist on instance "' + key + '"')
      }
    });

    return this;
  }

  static orderedBy(key, direction = 'desc') {
    const repo = getRepo(this.name);
    let callback = (a, b) => b[key] - a[key];

    if (direction !== 'desc') callback = (a, b) => b[key] + a[key];
    return repo.sort(callback);
  }

  static all() {
    return getRepo(this.name);
  }

  static first() {
    const records = getRepo(this.name);

    if (typeof records[0] !== 'undefined') {
      return new this(records[0]);
    }
    return null;
  }

  static insert(records) {
    if (!Array.isArray(records)) records = [records];

    let instances = [];
    records.forEach(record => {
      let instance = record;
      if (!(record instanceof Model)) {
        instance = Object.assign({}, new this(record));
      }
      instances.push(instance)
    });

    const done = !(instances.length === 0);
    if (done) {
      VuexStore.commit('schemasInsert', {
        schema: getSchemaName(this.name),
        instances,
      });
    }

    return done;
  }
}