import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

//Inline components
const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

const routes = [
  {
    path: '/fookoshimoto/pospoloto',
    name: 'foo',
    component: Foo,
  },
  {
    path: '/pas/pospoloto',
    name: 'bar',
    component: Bar,
  }
]

const router = new VueRouter({
  routes
})

export default router;