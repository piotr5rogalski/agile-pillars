import Vue from 'vue'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import Vuetify , {
  VCard,
  VImg,
  VCardTitle,
  VContent,
  VBtn,
  VCardActions,
  VCardText,
  VProgressCircular,
  VSpacer,
  VDialog,
  VDivider,
  VAlert,
  VApp,
} from 'vuetify/lib'

Vue.use(Vuetify, {
  components: {
    VCard,
    VImg,
    VCardTitle,
    VContent,
    VBtn,
    VCardActions,
    VCardText,
    VProgressCircular,
    VSpacer,
    VDialog,
    VDivider,
    VAlert,
    VApp,
  },
})
const opts = {
  icons: {
    iconfont: 'mdi',
  },
};

export default new Vuetify(opts)