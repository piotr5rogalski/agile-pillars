import Vue from 'vue'
import Vuex from 'vuex'
import storeData from "../store/index"

Vue.use(Vuex)

const store = new Vuex.Store(
  storeData
)

window.VuexStore = store

export default store