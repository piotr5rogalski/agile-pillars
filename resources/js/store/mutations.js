import {updateField} from "vuex-map-fields";

export default {
  updateField,

  // @payload {schema: '', record: Model}
  schemasInsert: ({schemas}, payload) => schemas[payload.schema] = schemas[payload.schema].concat(payload.record),

  // @payload {schema: '', record: Model}
  storeRecord: ({schemas}, payload) => schemas[payload.schema].push(payload.record),

  // @payload {schema: '', record: Model}
  saveRecord: ({schemas}, payload) => {
    payload.record.id
      ? VuexStore.commit('updateRecord', payload)
      : VuexStore.commit('storeRecord', payload);
  },

  // @payload {schema: '', record: Model}
  updateRecord: ({schemas}, payload) => {
    const data = payload.record;
    const schema = schemas[payload.schema];

    return schema.some((record, index) => {
      if(record.id === data.id) {
        schema[index] = {...record, ...data};
        return true;
      }
    })
  },
};