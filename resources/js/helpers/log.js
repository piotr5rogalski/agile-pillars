export default class Log {
  static info(message) {
    console.log(message);
  }

  static warning(message) {
    console.warn(message);
  }

  static error(error) {
    let showUrl = true;
    const showDetails = false;

    const haveConfig = typeof error.config !== 'undefined';
    if (!haveConfig || typeof error.config.url === 'undefined') {
      showUrl = false;
    }

    console.group('Error!')
    if (showUrl) {
      console.group(error.config.url)
    }

    if (showDetails) {
      console.error({error: error});
      console.log({send: error.config.data});
    } else {
      console.error(error.message);
    }

    if (showUrl) {
      console.groupEnd();
    }
    console.groupEnd();
  }

  static critical(message) {
    console.error(message);
  }

  static emergency(message) {
    console.error(message);
  }
}