<header class="v-sheet theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--is-scrolled deep-purple accent-4"
        data-booted="true" style="height: 48px; margin-top: 0; transform: translateY(0px); left: 0; right: 0;">
  <div class="v-toolbar__content" style="height: 48px;">
    <button type="button" class="v-app-bar__nav-icon v-btn v-btn--icon v-btn--round theme--dark v-size--default">
      <span class="v-btn__content">
        <span aria-hidden="true" class="v-icon notranslate theme--dark">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" class="v-icon__svg">
            <path d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z"></path>
          </svg>
        </span>
      </span>
    </button>

    <div class="v-toolbar__title">{{ config('app.name', 'Brand Name') }}</div>
    <div class="spacer"></div>
    <button type="button" class="v-btn v-btn--icon v-btn--round theme--dark v-size--default"><span
          class="v-btn__content">
        <i aria-hidden="true" class="v-icon notranslate mdi mdi-heart theme--dark"></i>
      </span>
    </button>
    <button type="button" class="v-btn v-btn--icon v-btn--round theme--dark v-size--default"><span
          class="v-btn__content">
        <i aria-hidden="true" class="v-icon notranslate mdi mdi-magnify theme--dark"></i></span></button>
    <div class="v-menu"></div>
    <button type="button" class="v-btn v-btn--icon v-btn--round theme--dark v-size--default" role="button"
            aria-haspopup="true" aria-expanded="false"><span class="v-btn__content">
        <i aria-hidden="true" class="v-icon notranslate mdi mdi-dots-vertical theme--dark"></i>
      </span>
    </button>
  </div>
</header>
