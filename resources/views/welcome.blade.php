@extends('layouts.app')

@section('content')
  <div class="container" style="width: 1200px">
    <div class="mx-auto column justify-content-center">
      <agile-value-container></agile-value-container>
      <div style="height: 50px"></div>
      <agile-principle-container></agile-principle-container>
      <vue-snotify></vue-snotify>
    </div>
  </div>
@endsection
