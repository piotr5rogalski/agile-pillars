<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'HomePage') }}</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
@section('navbar')
  <x-navbar></x-navbar>
@show

<main id="app">
  <v-app>
    <v-main>
      @yield('content')
    </v-main>
  </v-app>
</main>

<script src="{{ asset('js/laroute.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
