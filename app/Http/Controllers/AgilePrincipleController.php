<?php

namespace App\Http\Controllers;

use App\Models\AgilePrinciple;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AgilePrincipleController extends Controller
{

    /**
     * Returning a list of resources
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(AgilePrinciple::all()->toArray());
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $created = AgilePrinciple::create($request->all());

            return response()->json(['id' => $created->id]);
        } catch (\Throwable $e) {
            return response()->json(['error' => $e], Response::HTTP_CONFLICT);
        }
    }

    public function update(Request $request): JsonResponse
    {
        try {
            $data = $request->all();
            $updated = AgilePrinciple::find($data['id']);
            $updated->principle = $data['principle'];
            $updated->save();

            return response()->json(['updated' => $updated]);
        } catch (\Throwable $e) {
            return response()->json(['error' => $e], Response::HTTP_CONFLICT);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        try {
            $destroyed = AgilePrinciple::destroy($request->id);

            return response()->json(['status' => $destroyed]);
        } catch (\Throwable $e) {
            return response()->json(['error' => $e], Response::HTTP_CONFLICT);
        }
    }
}
