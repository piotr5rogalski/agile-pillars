const mix = require('laravel-mix');
const { VuetifyLoaderPlugin } = require('vuetify-loader')
require('vuetifyjs-mix-extension');

mix.extend('vuetify', new class {
  webpackConfig (config) {
    config.plugins.push(new VuetifyLoaderPlugin())
  }
});

mix.js('resources/js/app.js', 'public/js').vuetify().vue();
mix.sass('resources/sass/app.scss', 'public/css');
