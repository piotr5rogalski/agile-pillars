import { shallowMount } from '@vue/test-utils'
import AgilePrincipleContainer from '@/../resources/js/components/agile/AgilePrincipleContainer.vue'

describe('AgilePrincipleContainer.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(AgilePrincipleContainer, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
