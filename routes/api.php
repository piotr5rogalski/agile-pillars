<?php

use App\Http\Controllers\AgilePrincipleController;
use App\Http\Controllers\AgileValueController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/agile/values', AgileValueController::class)->only([
    'index', 'store', 'update', 'destroy'
]);

Route::resource('/agile/principles', AgilePrincipleController::class)->only([
    'index', 'store', 'update', 'destroy'
]);

