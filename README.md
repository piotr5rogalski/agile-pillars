# Agile pillars

Simple app about 4 agile values and 12 principles.

## Requirements
- Linux
- npm version 14
- php ^7.3
- composer v2

## Installing
```none
git clone git@gitlab.com:piotr5rogalski/agile-pillars.git
cd agile-pillars
npm run deploy
```
Last command run many things including server on: `http://localhost:8000/`
If not u can use: `php artisan serve`

## Seeding database
After running `npm run deploy` sqlite database file would be created:
`agile-pillars/database/database.sqlite`, migration will run and example faked data
would be inserted. If you want to clear tables just run:
`php artisan migrate:fresh` and if you want to seed them run `php artisan db:seed`

### App routing
List all available app routing, including api
`php artisan route:list`
