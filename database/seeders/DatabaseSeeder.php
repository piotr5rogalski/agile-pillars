<?php

namespace Database\Seeders;

use App\Models\AgilePrinciple;
use App\Models\AgileValue;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        AgileValue::factory(4)->create();
        AgilePrinciple::factory(12)->create();
    }
}
