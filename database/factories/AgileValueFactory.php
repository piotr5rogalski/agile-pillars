<?php

namespace Database\Factories;

use App\Models\AgileValue;
use Illuminate\Database\Eloquent\Factories\Factory;

class AgileValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AgileValue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => $this->faker->sentence,
            'over' => $this->faker->sentence,
        ];
    }
}
